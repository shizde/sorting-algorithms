# sorting-algorithms



## Sorting 

Sorting is the process of arranging available data in a particular format or following a predetermined rule.

## Importance of sorting 

- Search for itens in a array of itens can be done easier and quicker if this array is sorted;
- Presents data in a more readable format; 
- Help locate vairous patterns among the data; 
- Easy to spot duplicated values among the data available.

## Sort in Python

Currently, python has the method ```.sort()``` available for ordering lists. 
This method can order not only lists with integers but parsed strings, tuples and dictionaries.
For further information on it, [read the docs](https://docs.python.org/3/howto/sorting.html).

## Sorting Algorithms

### Bubble Sort

- In Bubble Sort Algorithm, the element on the n-th position is compared with the element on the n+1-th position;
- If the element on the n+1-th position is smaller than the element on the n-th position, both elements swap positions;
- The algorithm then proceeds to compare the element on the n+1-th position with the element on the n+2-th position and so on until the smallest element is in the first position of the list and the biggest element is at the last position of the list.

```python 
def bubble_sort(raw_list) -> list:
   swapped = False
    # Looping from size of array from last index[-1] to index [0]
    for n in range(len(raw_list)-1, 0, -1):
        for i in range(n):
            if raw_list[i] > raw_list[i + 1]:
                swapped = True
                # swapping data if the element is less than next element in the array
                raw_list[i], raw_list[i + 1] = raw_list[i + 1], raw_list[i]        
        if not swapped:
            # exiting the function if we didn't make a single swap
            # meaning that the array is already sorted.
            return 
```

### Insertion Sort

- The algorithm places a given element at the right position in a sorted list. So, in the beginning, we compare two elements and create a sorted list of two elementsn by comparing them;
- The algorithm proceeds to pick a third element and find its proper position among this sorted list;
- This process continues until all elements land in their proper positions.

```python
def insertion_sort(raw_list) -> list:
    n = len(raw_list)  # Get the length of the array
      
    if n <= 1:
        return  # If the array has 0 or 1 element, it is already sorted, so return
 
    for i in range(1, n):  # Iterate over the array starting from the second element
        key = raw_list[i]  # Store the current element as the key to be inserted in the right position
        j = i-1
        while j >= 0 and key < raw_list[j]:  # Move elements greater than key one position ahead
            raw_list[j+1] = raw_list[j]  # Shift elements to the right
            j -= 1
        raw_list[j+1] = key  # Insert the key in the correct position
```

### Merge Sort

- Divide and Conquer type of algorithm;
- Merge sort divides the list into equal parts and then sort the parts and unite them as a sorted list.

```python
def merge(arr, l, m, r):
    n1 = m - l + 1
    n2 = r - m
 
    # create temp arrays
    L = [0] * (n1)
    R = [0] * (n2)
 
    # Copy data to temp arrays L[] and R[]
    for i in range(0, n1):
        L[i] = arr[l + i]
 
    for j in range(0, n2):
        R[j] = arr[m + 1 + j]
 
    # Merge the temp arrays back into arr[l..r]
    i = 0     # Initial index of first subarray
    j = 0     # Initial index of second subarray
    k = l     # Initial index of merged subarray
 
    while i < n1 and j < n2:
        if L[i] <= R[j]:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1
        k += 1
 
    # Copy the remaining elements of L[], if there
    # are any
    while i < n1:
        arr[k] = L[i]
        i += 1
        k += 1
 
    # Copy the remaining elements of R[], if there
    # are any
    while j < n2:
        arr[k] = R[j]
        j += 1
        k += 1
 
# l is for left index and r is right index of the
# sub-array of arr to be sorted
 
 
def merge_sort(arr, l, r) -> list:
    if l < r:
 
        # Same as (l+r)//2, but avoids overflow for
        # large l and h
        m = l+(r-l)//2
 
        # Sort first and second halves
        mergeSort(arr, l, m)
        mergeSort(arr, m+1, r)
        merge(arr, l, m, r)

```

### Quick Sort

- Another Divide and Conquer type of algorithm;
- First, choose a pivot element in the array;
- Then stores the smaller elements in a list and the bigger elements in another list;
- Proceed with this method in the sublists until everything is sorted.

```python 
# Function to find the partition position
def partition(array, low, high):
 
    # choose the rightmost element as pivot
    pivot = array[high]
 
    # pointer for greater element
    i = low - 1
 
    # traverse through all elements
    # compare each element with pivot
    for j in range(low, high):
        if array[j] <= pivot:
 
            # If element smaller than pivot is found
            # swap it with the greater element pointed by i
            i = i + 1
 
            # Swapping element at i with element at j
            (array[i], array[j]) = (array[j], array[i])
 
    # Swap the pivot element with the greater element specified by i
    (array[i + 1], array[high]) = (array[high], array[i + 1])
 
    # Return the position from where partition is done
    return i + 1
 
# function to perform quicksort
 
def quick_sort(array, low, high) -> list:
    if low < high:
 
        # Find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = partition(array, low, high)
 
        # Recursive call on the left of pivot
        quickSort(array, low, pi - 1)
 
        # Recursive call on the right of pivot
        quickSort(array, pi + 1, high)
```



